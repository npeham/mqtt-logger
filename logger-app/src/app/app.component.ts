import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Level } from './level/shared/level'
import { LevelService } from './level/shared/level.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';


  private levels: Level[];

  private lightWz: boolean;
  private lightEz: boolean;
  private temperatureWz: any;
  private temperatureEz: any;
  private restUrl: string = "http://localhost:81";
  private locationOutdoor: boolean;

  private isUnderscoreLeft: boolean;
  private showTabs: boolean;


  constructor(private http: Http, private levelService: LevelService) {
    //this.loadLightStatus();
    //this.loadTemperature("", true);
    this.levels = levelService.getAllLevels();

    this.showTabs = true;
    this.isUnderscoreLeft = true;

    this.lightEz = false;
    this.lightWz = false;
    this.temperatureEz = "--.-";
    this.temperatureWz = "--.-";
  }

  public showOutdoor(): void {
    this.levelService.showOutdoor();
  }

  public showIndoor(): void {
    this.levelService.showIndoor();
  }

  private reloadTabs(): void {
    this.showTabs = false;
    setTimeout(x => {
      this.showTabs = true
    }, 1)
  }

  /*
  
    private loadLightStatus(): void {
      this.http.get(this.restUrl + "/light/eg/wz/").subscribe((res: Response) => {
        if (res.json().status === "false") {
          this.lightWz = false;
        }
        else if (res.json().status === "true") {
          this.lightWz = true;
        }
      });
      this.http.get(this.restUrl + "/light/eg/ez/").subscribe((res: Response) => {
        if (res.json().status === "false") {
          this.lightEz = false;
        }
        else if (res.json().status === "true") {
          this.lightEz = true;
        }
      });
    }
    private loadTemperature(room: string, bothLoad: boolean): void {
      if (room === "ez" || bothLoad) {
        this.http.get(this.restUrl + "/temperature/eg/ez/").subscribe((res: Response) => {
          this.temperatureEz = res.json().temperature;
        });
      }
      if (room === "wz" || bothLoad) {
        this.http.get(this.restUrl + "/temperature/eg/wz/").subscribe((res: Response) => {
          this.temperatureWz = res.json().temperature;
        });
      }
  
    }
  
    private turnLightOn(room: string): void {
      if (room === "wohnzimmer") {
        this.http.put(this.restUrl + "/light/eg/wz/true", "").subscribe();
        this.lightWz = true;
      }
      else if (room === "esszimmer") {
        this.http.put(this.restUrl + "/light/eg/ez/true", "").subscribe();
        this.lightEz = true;
      }
    }
  
    private turnLightOff(room: string): void {
      if (room === "wohnzimmer") {
        this.http.put(this.restUrl + "/light/eg/wz/false", "").subscribe();
        this.lightWz = false;
      }
      else if (room === "esszimmer") {
        this.http.put(this.restUrl + "/light/eg/ez/false", "").subscribe();
        this.lightEz = false;
      }
    }
    */
}
