//Author: Nico
var express = require('express');
var app = express();
var mqtt = require('mqtt')
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient

app.use(express.static(__dirname + "/logger-app/dist"));

app.use(bodyParser());
app.use(bodyParser.json());

app.listen(81, function () {
    console.log('Example app listening on port 81!');
});

var client = mqtt.connect('tcp://192.168.188.23:1883')

//Author: Julian
var db
var connection = 'mongodb://localhost:27017/mqtt_nvs'

MongoClient.connect(connection, (err, database) => {
    if (err) return console.log(err)
    db = database
    app.listen(27017, () => {
        console.log('listening on port 27017')
    })
})

var collectionName = 'sensorData';

//Author Julian
client.on('connect', function () {
    client.subscribe('+/+/+/+');
})

//Author: Julian
client.on('message', function (topic, message) {
    var values = topic.split('/');

    message = JSON.parse(message);

    db.collection(collectionName).findOne({ loc: values[0], floor: values[1], room: values[2], sensor: values[3] }, function (err, results) {
        if (results) {
            db.collection(collectionName).update(
                {
                    _id: results._id
                },
                {
                    $push: { message: { value: message.value, unit: message.unit, timestamp: message.timestamp } }
                }
            )
        }
        else {
            db.collection(collectionName).insertOne({
                "loc": values[0],
                "floor": values[1],
                "room": values[2],
                "sensor": values[3],
                "message" : [{
                    "value": message.value,
                    "unit": message.unit,
                    "timestamp": message.timestamp
                }]
            })
        }
    })
})

//Author: Nico/Julian
app.post('/publish', function (req, res) {
    var topic = req.body.topic;
    var message = req.body.message;

    console.log(topic);
    console.log(message);

    client.publish(topic, JSON.stringify(message));
    res.end();
});

//Author: Julian
app.get('/allData', function (req, res) {
    db.collection(collectionName).find().toArray(function (err, results) {
        res.json(results);
    })
});

//Author: Julian
app.put('/test', function (req, res) {
    topic = req.body.topic;
    message = req.body.message;

    var values = topic.split('/');

    db.collection(collectionName).findOne({ loc: values[0], floor: values[1], room: values[2], sensor: values[3] }, function (err, results) {
        if (results) {
            db.collection(collectionName).update(
                {
                    _id : results._id
                },
                {
                    $push: { message: { value: message.value, unit: message.unit, timestamp: message.timestamp } }
                }
            )
        }
        else {
            db.collection(collectionName).insertOne({
                "loc": values[0],
                "floor": values[1],
                "room": values[2],
                "sensor": values[3],
                "message" : [{
                    "value": message.value,
                    "unit": message.unit,
                    "timestamp": message.timestamp
                }]
            })
        }
    })

    res.end();
});