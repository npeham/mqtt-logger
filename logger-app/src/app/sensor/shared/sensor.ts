export class Sensor {
    
    private id : number;
    private name : string;
    private unit : any;
    private value : string;
    
    /**
     *
     */
    constructor(name:string,unit:any) {      
        this.name= name;
        this.unit = unit;
            
    }
    
     public get Name() : string {
        return this.name;
    }
    
    
    public get Id() : number {
        return this.id;
    }
    
    
    public set Id(v : number) {
        this.id = v;
    }

    public get Unit() : any{
        return this.unit;
    }


    public set Value(v : string) {
        this.value = v;
    }

    public get Value() : string{
        return this.value;
    }

    
}
