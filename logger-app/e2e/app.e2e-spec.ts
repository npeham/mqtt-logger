import { LoggerAppPage } from './app.po';

describe('logger-app App', function() {
  let page: LoggerAppPage;

  beforeEach(() => {
    page = new LoggerAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
