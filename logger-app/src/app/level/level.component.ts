import { Component, OnInit, EventEmitter } from '@angular/core';
import {  Room } from '../room/shared/room'
import { Sensor } from "../sensor/shared/sensor"
import { LevelService } from "./shared/level.service"
import { Level} from './shared/level'


@Component({
  selector: 'mqtt-level',
  templateUrl: './level.component.html',
  styleUrls: ['./level.component.css'],
  inputs:["level"]
})
export class LevelComponent implements OnInit {

private rooms : Room[];
private sensors: Sensor[];
private level : Level;


  constructor(private levelService:LevelService) {
    this.rooms = [];
    this.sensors= []  
   
   }

  
  public callRequestInService( path : string) {
    this.levelService.sendRequestForPublish(this.level.Name+"/"+path);
  }

  ngOnInit() {
    
    
  }

}
