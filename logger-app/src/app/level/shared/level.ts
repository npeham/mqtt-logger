import { Room } from "../../room/shared/room";


export class Level {
    
    
    private id : number;
    private name : string;
    private rooms : Room[];
    
    
    /**
     *
     */
    constructor(name:string) {
        
           this.name = name;
           this.rooms = [];
    }
    
    
    public get Name() : string {
        return this.name;
    }
    
    
    public get Rooms() : Room[] {
        return this.rooms;
    }
    
    
    public set Rooms(v : Room[]) {
        this.rooms = v;
    }
    
    
    public get Id() : number {
        return this.id;
    }
    
    
    public set Id(v : number) {
        this.id = v;
    }
    
    
    
    
    
    
    
    
}
