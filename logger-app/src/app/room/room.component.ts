import { Component, OnInit, EventEmitter } from '@angular/core';
import {Sensor} from "../sensor/shared/sensor"
import {  Room } from '../room/shared/room'

@Component({
  selector: 'mqtt-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
  inputs:["room"],
  outputs:["onEmit"]
})
export class RoomComponent implements OnInit {


private room : Room;
private sensors: Sensor[];
private onEmit : EventEmitter<string>;

  constructor(){ 
     this.sensors=[];
     this.onEmit = new EventEmitter<string>();
  }

private addPathToEmitData(lightStatus : string) : void{
  this.onEmit.emit(this.room.Name+"/"+lightStatus);
}

  ngOnInit() {
  }

}
