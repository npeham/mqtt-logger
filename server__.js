var express = require('express');
var app = express();
var mqtt = require('mqtt')
var bodyParser = require('body-parser');

app.use(express.static(__dirname + "/logger-app/dist"));

app.use(bodyParser());
app.use(bodyParser.json());

app.listen(81, function () {
  console.log('Example app listening on port 81!');
});

//OLD DOCUMENT!!!!
//FOR "TEMPLATING"-USE ONLY

//mqtt client
var client = mqtt.connect('mqtt://localhost')

client.on('connect', function () {
  // client.subscribe('presence');
  // client.publish('presence', 'Hello mqtt')
  client.subscribe('temperature/eg/wz');
  client.subscribe('temperature/eg/ez');
  client.subscribe('light/eg/wz');
  client.subscribe('light/eg/ez');
  // client.publish('temperature/eg/ez', '23.3')
})

client.on('message', function (topic, message) {
  // message is Buffer 
  if (topic === "temperature/eg/wz") {
    temperatureWz = message.toString();
  }
  else if (topic === "temperature/eg/ez") {
    temperatureEz = message.toString();
  }
  else if (topic === "light/eg/ez") {
    lightEz = message.toString();
  }
  else if (topic === "light/eg/wz") {
    lightWz = message.toString();
  }
  // client.end()
})


var temperatureWz = 23.8;
var temperatureEz = 23.8;
var lightEz = false;
var lightWz = false;



//rest publish
app.put('/publish', function (req, res) {
  var topic = req.body.topic;
  var message = req.body.message;

  client.publish(topic, message);
  res.end();
});

//rest routes
// app.put('/temperature/:stage/:room/:newTemp', function (req, res) {
//   var stage = req.params.room;
//   var room = req.params.room;
//   var newTemp = req.params.newTemp;

//   if (room === "wz") {
//     temperatureWz = newTemp;
//   }
//   else if (room === "ez") {
//     temperatureEz = newTemp;
//   }
//   res.end();
// });
app.get('/temperature/:stage/:room/', function (req, res) {
  var stage = req.params.stage;
  var room = req.params.room;

  if (room === "wz") {
    res.json({ "temperature": temperatureWz });
  }
  else if (room === "ez") {
    res.json({ "temperature": temperatureEz });
  }
});

app.put('/light/:stage/:room/:status', function (req, res) {
  var stage = req.params.stage;
  var room = req.params.room;
  var status = req.params.status;

  if (room === "wz") {
    lightWz = status;
  }
  else if (room === "ez") {
    lightEz = status;
  }
  res.end();
});
app.get('/light/:stage/:room/', function (req, res) {
  var stage = req.params.room;
  var room = req.params.room;

  if (room === "wz") {
    res.json({ "status": lightWz });
  }
  else if (room === "ez") {
    res.json({ "status": lightEz });
  }
});