import { Component, OnInit, EventEmitter } from '@angular/core';
import {Sensor} from "../sensor/shared/sensor"


@Component({
  selector: 'mqtt-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.css'],
  inputs:["sensor"],
  outputs:["onLightChange"]
})
export class SensorComponent implements OnInit {

private sensor : Sensor;
private onLightChange : EventEmitter<string>;

  constructor() {
    this.onLightChange = new EventEmitter<string>();
   }

  private turnLightOn(): void {
    this.sensor.Value="1";
    this.onLightChange.emit(this.sensor.Name+"/"+"1");
  }

  private turnLightOff(): void {
    this.sensor.Value="0";
    this.onLightChange.emit(this.sensor.Name+"/"+"0");
  }

  ngOnInit() {
  }

}
