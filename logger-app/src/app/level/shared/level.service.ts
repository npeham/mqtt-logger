import { Injectable } from '@angular/core';
import { Level } from './level'
import { Room } from '../../room/shared/room'
import { Sensor } from "../../sensor/shared/sensor"
import { Http, Response, Headers } from '@angular/http';

@Injectable()
export class LevelService {

  private levels: Level[];
  private rooms: Room[];
  private sensors: Sensor[];
  private restUrl: string = "http://localhost:81/allData";
  //private restUrl: string = "http://localhost:4200/TestData/message.json";
  private requestUrl : string = "http://localhost:81/"
  private levelIndoor: Level[];
  private levelOutdoor: Level[];
  private isIndoor: boolean;


  constructor(private http: Http) {
    this.levels = [];
    this.loadLevels();
    this.isIndoor = false;

  }

  public getAllLevels(): Level[] {
    return this.levels;
  }

  public showOutdoor(): void {
    this.isIndoor = false;
    this.loadLevels();
  }

  public showIndoor(): void {
    this.isIndoor = true;
    this.loadLevels();
  }

  public sendRequestForPublish(path : string) {
    let topic : string;
    let value : string = path.charAt(path.length-1);

    if(this.isIndoor == true){
        topic =  "innen"+"/"+path.slice(0,path.length-2);
    }
    else{
      topic = "außen"+"/"+path.slice(0,path.length-2);
    }

    let jsonBody = { 
      topic: topic, 
      message: { 
          value : value,
          unit : "",
          timestamp : "01.01.2001 00:00:00",

      }
    }

    const headers = new Headers({ 'Content-Type': 'application/json' });
    this.http.post(this.requestUrl+"publish",JSON.stringify(jsonBody), {
      headers: headers
    }).subscribe();

    
  }


  private loadLevels(): void {
    this.levels = [];
    this.http.get(this.restUrl).subscribe((res: Response) => {

      var js = res.json();

      js.forEach(jsX => {
        if (this.isIndoor == true && jsX.loc == "innen") {
          this.saveSensor(jsX);

        }
        else if (jsX.loc == "außen") {
          this.saveSensor(jsX);

        }
      })




/// Alter Response
      /*
      let actualLocation: any;
      if (this.isIndoor == true) {
        actualLocation = js.loc[0]
      }
      else {
        actualLocation = js.loc[1];
      }
     
        actualLocation.floor.forEach(levelX => {
          let level: Level;
          level = new Level(levelX.name);

          levelX.room.forEach(roomX => {
            let room: Room;
            room = new Room(roomX.name);

            roomX.sensor.forEach(sensorX => {
              let sensor: Sensor;
              sensor = new Sensor(sensorX.name, sensorX.message[sensorX.message.length - 1].unit)
              sensor.Value = sensorX.message[sensorX.message.length - 1].value;
              console.log(sensor.Value);
              room.addSensor(sensor);
            });
            level.Rooms.push(room);
          });
          this.levels.push(level);
        });
      */});
  }


  private saveSensor(jsX): void {

    let level: Level = null;
    let room: Room = null;
    let sensor: Sensor = null;

    let newLevel: boolean = true;
    let newRoom: boolean = true;
    let newSensor: boolean = true;

    let actlevel: Level = null;
    let actroom: Room = null;


    //let actsensor : Sensor;

    // console.log("LEVELS", this.levels);
    this.levels.forEach(levelX => {

      if (levelX.Name == jsX.floor) {
        actlevel = levelX;
        

        levelX.Rooms.forEach(roomX => {
          let roomY = roomX;
          console.log("komischa",roomX);
          if (roomX.Name == jsX.room) {
            console.log("roomX.Name: ", roomX.Name);
            console.log("jsX.room: ", jsX.room);

            actroom = roomX;
            //actroom = new Room(roomX.Name);

            roomX.Sensors.forEach(sensorX => {
              if (sensorX.Name == jsX.sensor)
                newSensor = false;
            })
            newRoom = false;
          }
        })
        newLevel = false;
      }
    });


    sensor = new Sensor(jsX.sensor, jsX.message[jsX.message.length - 1].unit);
    sensor.Value = jsX.message[jsX.message.length - 1].value;



    if (actroom == null) {
      room = new Room(jsX.room);
      room.addSensor(sensor);
      // console.log("HALLO",room);
    }
    else if (actroom != null) {
      // console.log("HALLO",actroom);
      actroom.addSensor(sensor);
    }

    if (actlevel == null) {
      level = new Level(jsX.floor);
      level.Rooms.push(room);
      this.levels.push(level);
       console.log("schauma null", jsX.floor);
      // console.log("HALLO", this.levels);
    }
    else if (actlevel != null) {
      console.log("schauma");
      if(room !== null) {
        actlevel.Rooms.push(room);
      }
      
      // this.levels.push(actlevel);
    }
  }
}
