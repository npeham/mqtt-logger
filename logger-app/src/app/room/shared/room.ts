import {Sensor} from "../../sensor/shared/sensor"

export class Room {
    
    private id : number;
    private name : string;
    private sensors : Sensor[];
    
    /**
     *
     */
    constructor(name:string) {
        this.name= name;
        this.sensors =[];
            
    }
    
    
    
    public get Name() : string {
        return this.name;
    }
    
    
    public get Sensors() : Sensor[] {
        return this.sensors;
    }
    
    public get Id() : number {
        return this.id;
    }
    
    
    public set Id(v : number) {
        this.id = v;
    }

    public addSensor(sensor:Sensor):void{
        this.sensors.push(sensor);
    }
    
    
}
