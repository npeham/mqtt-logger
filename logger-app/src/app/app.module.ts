import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';

import { AppComponent } from './app.component';
import { LevelComponent } from './level/level.component';
import { RoomComponent } from './room/room.component';
import { SensorComponent } from './sensor/sensor.component';

import { LevelService } from './level/shared/level.service';


@NgModule({
  declarations: [
    AppComponent,
    LevelComponent,
    RoomComponent,
    SensorComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot()
  ],
  providers: [LevelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
